<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../application/entities/Dish.php';
require __DIR__ . '/../application/entities/Ingredient.php';
require __DIR__ . '/../application/forms/SearchForm.php';
require __DIR__ . '/../application/services/dish/DishSearchService.php';
