<?php

namespace tests\unit;

use Yii;
use app\application\forms\SearchForm;
use app\application\entities\Dish;
use app\application\entities\Ingredient;
use app\application\services\dish\DishSearchService;

class SearchDishTest extends \Codeception\PHPUnit\TestCase
{
    private static $search_model;
    private static $search_service;


    private const FULL_OVERLAP_SET = [
        'ingredients'=>[
            'ingredient_one'=>'potato',
            'ingredient_two'=>'tomato',
            'ingredient_three'=>'cucumber',
            'ingredient_four'=>'',
            'ingredient_five'=>'',
        ],
        'quant_overlaps'=>2
    ];


    private const PARTIAL_OVERLAP_SET = [
        'ingredients'=>[
            'ingredient_one'=>'potato',
            'ingredient_two'=>'tomato',
            'ingredient_three'=>'dill',
            'ingredient_four'=>'beef',
            'ingredient_five'=>'',
        ],
        'quant_overlaps'=>4
    ];


    private const NONE_OVERLAP_SET = [
        'ingredients'=>[
            'ingredient_one'=>'apple',
            'ingredient_two'=>'milk',
            'ingredient_three'=>'chicken',
            'ingredient_four'=>'',
            'ingredient_five'=>'',
        ],
        'quant_overlaps'=>0
    ];


    public static function setUpBeforeClass():void
    {
        $config = require __DIR__ . '/../../config/web.php';
        Yii::$app = new \yii\web\Application($config);

        static::$search_model = new SearchForm();
        static::$search_service = new DishSearchService();
    }


    public function testFullOverlaps()
    {
        $this->launchCompareQuantOverlaps(static::FULL_OVERLAP_SET['ingredients'], static::FULL_OVERLAP_SET['quant_overlaps']);
    }


    public function testPartialOverlaps()
    {
        $this->launchCompareQuantOverlaps(static::PARTIAL_OVERLAP_SET['ingredients'], static::PARTIAL_OVERLAP_SET['quant_overlaps']);
    }


    public function testNoneOverlaps()
    {

        $this->launchCompareQuantOverlaps(static::NONE_OVERLAP_SET['ingredients'], static::NONE_OVERLAP_SET['quant_overlaps']);
    }


    private function launchCompareQuantOverlaps($test_ingredients_set, $waiting_quant_overlaps)
    {
        $this->initializeSearchModel(static::$search_model, $test_ingredients_set);

        static::$search_service->findSuitableDishes(static::$search_model);
        $current_quant_overlaps = count(static::$search_model->getDishesList()??[]);
        $this->clearSearchModelIngredientsList(static::$search_model);

        $this->assertEquals($waiting_quant_overlaps, $current_quant_overlaps);
    }


    private function initializeSearchModel($search_model, $ingredients_list)
    {
        $search_model->ingredient_one = $ingredients_list['ingredient_one'];
        $search_model->ingredient_two = $ingredients_list['ingredient_two'];
        $search_model->ingredient_three = $ingredients_list['ingredient_three'];
        $search_model->ingredient_four = $ingredients_list['ingredient_four'];
        $search_model->ingredient_five = $ingredients_list['ingredient_five'];
    }


    private function clearSearchModelIngredientsList($search_model)
    {
        $search_model->setDishesFindResults(null);
    }
}
