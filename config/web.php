<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'Testing task',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'app\config\bootstrap\SetUp'
    ],
    'language' => 'ru_RU',
    'charset' => 'UTF-8',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'baseUrl'=>'',
            'cookieValidationKey' => 'a67af4f0916989a99c8216e6549e6fa001d370bc',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\application\entities\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'books'=>'book/list',
                'book/<id:\d+>'=>'book/single',
                'login'=>'site/login',
                'logout'=>'site/logout',
                'admin'=>'admin/index',
                'dish/<id:\d+>'=>'dish/view',
                'dish/create'=>'dish/create',
                'dish/update/<id:\d+>'=>'dish/update',
                'dish/delete/<id:\d+>'=>'dish/delete',
                'dish/save'=>'dish/save',
                'ingredient/create'=>'ingredient/create',
                'ingredient/update/<id:\d+>'=>'ingredient/update',
                'ingredient/delete/<id:\d+>'=>'ingredient/delete',
                'search'=>'site/search',
                'index'=>'site/index',
                ''=>'site/index'
            ],
        ],

    ],
    'params' => $params,
];

return $config;
