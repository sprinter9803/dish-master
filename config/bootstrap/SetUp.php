<?php

namespace app\config\bootstrap;

use yii\base\BootstrapInterface;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app)
    {
        define('SIMPLE_DISH_COMPLEXITY', 1);
        define('MEDIUM_DISH_COMPLEXITY', 2);
        define('HARD_DISH_COMPLEXITY', 3);
        define('MIN_QUANTITY_OF_INGREDIENTS', 2);

        define('RESPONSE_ACTION_RENDER', 'render');
        define('RESPONSE_ACTION_REDIRECT', 'redirect');
    }
}
