<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;

class FindModelBehavior extends Behavior
{
    protected const ENTITY_MESSAGE_MAP = [
        'Dish'=>'Requested dish not exist',
        'Ingredient'=>'Requested ingredient not exist',
    ];

    public function findModel($id, $entity)
    {
        $short_class_name = $this->getShortClassName($entity->className());

        if (($find_model = ($entity)::findOne($id)) !== null) {
            return $find_model;
        } else {
            throw new NotFoundHttpException(static::ENTITY_MESSAGE_MAP[$short_class_name]);
        }
    }

    private function getShortClassName($full_class_name)
    {
        $parts = explode('\\',$full_class_name);
        return $parts[count($parts)-1];
    }
}
