<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;

class ResponseActionBehavior extends Behavior
{
    public function responseAction($service_response)
    {
        switch ($service_response['type']) {
            case RESPONSE_ACTION_RENDER:
                return $this->owner->render($service_response['view'], $service_response['params']);
                break;
            case RESPONSE_ACTION_REDIRECT:
                return $this->owner->redirect($service_response['url']);
                break;
            default:
                return null;
                break;
        }
    }
}
