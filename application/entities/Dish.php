<?php

namespace app\application\entities;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;

class Dish extends \yii\db\ActiveRecord
{
    public $choice_ingredient;

    private const SIZE_DISH_LIST_PAGINATION = 10;

    public static function tableName()
    {
        return 'dish';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT=>['name', 'choice_ingredient']
        ];
    }

    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id'=>'ingredient_id'])
                ->viaTable('dish_ingredient', ['dish_id'=>'id']);
    }

    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id'=>'book_id']);
    }

    public static function getSuitableDishList($list_ingredients_id)
    {
        $dish_suitable_list = (new Query())->select('di.dish_id, d.name, COUNT(di.ingredient_id) as quant')
            ->from('dish_ingredient di')
            ->where(['in', 'ingredient_id', $list_ingredients_id])
            ->andWhere(['not in', 'd.id',
                    (new Query())->select('dish_id')
                                          ->from('dish_ingredient')
                                          ->where(['in','ingredient_id',
                            (new Query())->select('id')
                                                 ->from('ingredient')
                                                 ->where(['=','hidden', true])
                             ])
                     ])
            ->groupBy(['di.dish_id'])
            ->having(['>', 'COUNT(di.ingredient_id)', 1])
            ->join('LEFT JOIN', 'dish d', 'd.id=di.dish_id')
            ->join('LEFT JOIN', 'ingredient i', 'i.id=di.ingredient_id')
            ->orderBy('quant DESC')
            ->all();

        return $dish_suitable_list;
    }

    public static function getIngredientId($name)
    {
        return (Ingredient::findByName($name)->id) ?? null;
    }

    public static function transactionForAdd($action_params)
    {
        $new_dish = new Dish();

        $transaction = $new_dish->getDb()->beginTransaction();

            $new_dish->setAttribute('name', $action_params['dish_name']);
            $new_dish->save(false);

            foreach ($action_params['dish_ingredients'] as $ingredient) {

                $link_query = Yii::$app->db->createCommand('INSERT INTO dish_ingredient (id, dish_id, ingredient_id) VALUES (default, :dish_id, :ingredient_id)')
                                    ->bindValue(':dish_id', $new_dish->id)
                                    ->bindValue(':ingredient_id', $ingredient);

                if (!$link_query->execute()) {
                    $transaction->rollback();
                    return '/admin?message='.static::ERROR_ON_PROCESS_ADD;
                }
            }

        $transaction->commit();
    }

    public function getIngredientsIds()
    {
        $ingredients_info_set = [];

        $ingredients_data = $this->getIngredients()->all();

        foreach ($ingredients_data as $ingredient) {
            $ingredients_info_set[] = $ingredient->id ?? null;
        }

        return $ingredients_info_set;
    }


    public static function getDishProvider()
    {
        $query_dish = Dish::find();

        return new ActiveDataProvider([
            'query' => $query_dish,
            'pagination' => [
                'pageSize' => static::SIZE_DISH_LIST_PAGINATION
            ]
        ]);
    }

    public static function getDishData($id)
    {
        $info = static::findOne($id);
        $book = (static::findOne($id))->getBook()->one();
        $ingredients = (static::findOne($id))->getIngredients()->all();

        return [
            'book'=>$book,
            'info'=>$info,
            'ingredients'=>$ingredients
        ];
    }
}
