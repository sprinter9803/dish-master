<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;

class Author extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'author';
    }
}
