<?php

namespace app\application\entities;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;

class Book extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'book';
    }

    public static function getFullBooksList()
    {
        $books = static::find()->all();
        return $books;
    }

    public static function getBookDishes($book_id)
    {
        $dishes = (static::findOne($book_id))->getDishes()->all();
        return $dishes;
    }

    public static function getBookName($id)
    {
        return static::findOne($id)->name;
    }

    public function getDishes()
    {
        return $this->hasMany(Dish::className(), ['book_id'=>'id']);
    }

    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id'=>'author_id']);
    }
}
