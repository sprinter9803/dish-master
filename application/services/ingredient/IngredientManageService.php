<?php

namespace app\application\services\ingredient;

use Yii;
use yii\base\Component;
use app\application\entities\Ingredient;
use app\behaviors\FindModelBehavior;

class IngredientManageService extends Component
{
    private const HAVE_DISHES_FOR_DELETE_INGREDIENT = "Удаление ингредиента недоступно, имеются блюда где он задействован. Устраните его из них";

    private const INGREDIENT_SUCCESS_DELETE = "Ингредиент успешно удален";

    private const INGREDIENT_SUCCESS_CREATE = "Ингредиент успешно добавлен";

    private const INGREDIENT_SUCCESS_UPDATE = "Ингредиент успешно отредактирован";

    public function behaviors()
    {
        return [
            'FindModelBehavior'=>FindModelBehavior::className()
        ];
    }

    public function createNewIngredient()
    {
        $ingredient_model = new Ingredient();
        return $this->manageAction($ingredient_model, [
            'view_name'=>'create',
            'success_message'=>static::INGREDIENT_SUCCESS_CREATE
        ]);
    }

    public function updateDataForIngredient($ingredient_id)
    {
        $ingredient_model = $this->findModel($ingredient_id, new Ingredient());
        return $this->manageAction($ingredient_model, [
            'view_name'=>'update',
            'success_message'=>static::INGREDIENT_SUCCESS_UPDATE
        ]);
    }

    public function deleteIngredient($ingredient_id)
    {
        $linked_dishes = $this->findModel($ingredient_id, new Ingredient())->getDishes()->all();
        if (count($linked_dishes) > 0) {
            return ['/admin?message='.static::HAVE_DISHES_FOR_DELETE_INGREDIENT];
        } else {
            $this->findModel($ingredient_id, new Ingredient())->delete();
            return ['/admin?message='.static::INGREDIENT_SUCCESS_DELETE];
        }
    }

    private function manageAction($ingredient_model, $action_params)
    {
        if ($ingredient_model->load(Yii::$app->request->post()) && $ingredient_model->save()) {
            return [
                'type'=>RESPONSE_ACTION_REDIRECT,
                'url'=>'/admin?message='.$action_params['success_message']
            ];
        } else {
            return [
                'type'=>RESPONSE_ACTION_RENDER,
                'view'=>$action_params['view_name'],
                'params'=>['ingredient_model' => $ingredient_model]
            ];
        }
    }
}
