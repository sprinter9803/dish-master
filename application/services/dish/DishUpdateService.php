<?php

namespace app\application\services\dish;

use Yii;
use yii\base\Component;
use app\application\entities\Dish;
use app\application\entities\Ingredient;
use app\behaviors\FindModelBehavior;

class DishUpdateService extends Component
{
    private const UPDATE_DISH_WITHOUT_NAME = 'Ошибка при обновлении. У блюда должно быть название';

    private const TOO_FEW_INGREDIENTS_FOR_UPDATE_DISH = 'Ошибка при обновлении. У блюда остается слишком мало ингредиентов';

    public function behaviors()
    {
        return [
            'FindModelBehavior'=>FindModelBehavior::className()
        ];
    }

    public function getUpdateWindowData($id)
    {
        $dish_model = $this->findModel($id, new Dish());

        $update_dish_ingredients_list = $dish_model->getIngredientsIds();
        $message = Yii::$app->request->get()['message'] ?? null;

        if ($dish_model->load(Yii::$app->request->post())) {

            $add_results = $this->checkAddIngredientToDish($dish_model, $update_dish_ingredients_list);

            if (!$add_results['success']) {
                return [
                    'type'=>RESPONSE_ACTION_REDIRECT,
                    'url'=>'/dish/update/'.$id.'?message='.static::UPDATE_DISH_WITHOUT_NAME
                ];
            }

            $update_dish_ingredients_list = $add_results['modified_dish_ingredients_list'];

            return [
                'type'=>RESPONSE_ACTION_RENDER,
                'view'=>'update',
                'params'=>[
                    'available_ingredient_list'=>Ingredient::getAvailableIngredientList($update_dish_ingredients_list),
                    'dish_model'=>$dish_model,
                    'manage_dish_ingredients_list'=>Ingredient::mergeIngredientNamesOnIds($update_dish_ingredients_list),
                    'message'=>$message
                ]
            ];

        } else {

            $delete_results = $this->checkDeleteIngredientFromDish($dish_model, $update_dish_ingredients_list);

            if (!$delete_results['success']) {
                return [
                    'type'=>RESPONSE_ACTION_REDIRECT,
                    'url'=>'/dish/update/'.$id.'?message='.static::TOO_FEW_INGREDIENTS_FOR_UPDATE_DISH
                ];
            }

            $update_dish_ingredients_list = $delete_results['modified_dish_ingredients_list'];

            return [
                'type'=>RESPONSE_ACTION_RENDER,
                'view'=>'update',
                'params'=>[
                    'available_ingredient_list'=>Ingredient::getAvailableIngredientList($update_dish_ingredients_list),
                    'dish_model'=>$dish_model,
                    'manage_dish_ingredients_list'=>Ingredient::mergeIngredientNamesOnIds($update_dish_ingredients_list),
                    'message'=>$message
                ]
            ];
        }
    }

    public function checkDeleteIngredientFromDish($dish_model, $update_dish_ingredients_list)
    {
        $modified_dish_ingredients_list = $update_dish_ingredients_list;

        if (!empty(Yii::$app->request->get()['discard'])) {

            $delete_ingredient_id = Yii::$app->request->get()['discard'];

            $key = array_search($delete_ingredient_id, $modified_dish_ingredients_list);
            if ($key !== false) {

                if (count($modified_dish_ingredients_list)>MIN_QUANTITY_OF_INGREDIENTS) {

                    Ingredient::deleteIngredientFromDish([
                        'dish_id'=>$dish_model->id,
                        'ingredient_id'=>$delete_ingredient_id,
                    ]);

                    unset($modified_dish_ingredients_list[$key]);

                    return [
                        'success'=>true,
                        'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
                    ];

                } else {

                    return [
                        'success'=>false,
                        'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
                    ];
                }
            }
        }

        return [
            'success'=>true,
            'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
        ];
    }

    public function checkAddIngredientToDish($dish_model, $update_dish_ingredients_list)
    {
        $modified_dish_ingredients_list = $update_dish_ingredients_list;

        if (!empty($dish_model->name)) {

            $dish_model->save();

        } else {

            return [
                'success'=>false,
                'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
            ];
        }

        if (!empty($dish_model->choice_ingredient)) {
            Ingredient::addIngredientToDish([
                'dish_id'=>$dish_model->id,
                'ingredient_id'=>$dish_model->choice_ingredient
            ]);

            $modified_dish_ingredients_list[] = $dish_model->choice_ingredient;
        }

        return [
            'success'=>true,
            'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
        ];
    }
}
