<?php

namespace app\application\services\dish;

use Yii;
use yii\base\Component;
use app\application\forms\SearchForm;
use app\application\entities\Dish;

class DishSearchService extends Component
{
    private const NO_DISH_FIND = 'Ничего не найдено';

    private const EXIST_FULL_OVERLAP = 'Есть полные совпадения ингредиентов';

    private const EXIST_PART_OVERLAP = 'Нет полных, но есть частичные совпадения ингредиентов';

    public function getSearchWindowDataContent()
    {
        $search_model = new SearchForm();

        $search_model->load(Yii::$app->request->post());
        if ($search_model->load(Yii::$app->request->post()) && $this->findSuitableDishes($search_model)) {

            return [
                'search_model' => $search_model,
                'search_data' => $search_model->getDishesList(),
                'search_message' => $search_model->getMessage()
            ];

        } else {

            return [
                'search_model' => $search_model,
                'search_data' => [],
                'search_message' => $search_model->getMessage()
            ];
        }
    }

    public function findSuitableDishes($search_model)
    {
        if (($dishes_find_result = $this->searchSuitable($search_model->getAttributes())) !== null) {

            $search_model->setMessage($dishes_find_result['message']);
            $search_model->setDishesFindResults($dishes_find_result);
            return true;
        }

        $search_model->setMessage(static::NO_DISH_FIND);

        return false;
    }

    public function searchSuitable($search_params)
    {
        $list_ingredients_id = $this->getIngredientsIdsOnNames($search_params);

        $dish_list = Dish::getSuitableDishList($list_ingredients_id);

        if (!count($dish_list)) {
            return null;
        }

        $full_dish_list = $this->getFullDishList($dish_list);

        if (count($full_dish_list)) {
            return [
                'message'=>static::EXIST_FULL_OVERLAP,
                'dishes_list'=>$full_dish_list
            ];
        } else {
            return [
                'message'=>static::EXIST_PART_OVERLAP,
                'dishes_list'=>$this->getPartiableDishList($dish_list)
            ];
        }
    }

    private function getIngredientsIdsOnNames($search_names)
    {
        $list_ingredients_id = [];

        foreach ($search_names as $ingredient_name) {
            $search_id = Dish::getIngredientId(trim($ingredient_name));
            if (!empty($search_id)) {
                $list_ingredients_id[] = $search_id;
            }
        }

        return $list_ingredients_id;
    }

    private function getFullDishList($dish_list)
    {
        $full_dish_list = [];

        for ($i=0; $i<count($dish_list); $i++) {

            $ingredients = (Dish::findOne($dish_list[$i]['dish_id']))->getIngredients()->all();

            if (count($ingredients) === (int)$dish_list[$i]['quant']) {

                $full_dish_list[] = [
                    'id'=>$dish_list[$i]['dish_id'],
                    'name'=>$dish_list[$i]['name'],
                    'ingredients'=>$this->getIngredientStringList($ingredients)
                ];
            }
        }

        return $full_dish_list;
    }

    private function getPartiableDishList($dish_list)
    {
        $partiable_dish_list = [];

        for ($i=0; $i<count($dish_list); $i++) {

            $ingredients = (Dish::findOne($dish_list[$i]['dish_id']))->getIngredients()->all();

            $partiable_dish_list[] = [
                'id'=>$dish_list[$i]['dish_id'],
                'name'=>$dish_list[$i]['name'],
                'ingredients'=>$this->getIngredientStringList($ingredients)
            ];
        }

        return $partiable_dish_list;
    }

    private function getIngredientStringList($ingredients_record)
    {
        $ingredients_set = [];

        foreach ($ingredients_record as $ingredient) {
            $ingredients_set[] = $ingredient->name;
        }

        return implode(', ', $ingredients_set);
    }
}
