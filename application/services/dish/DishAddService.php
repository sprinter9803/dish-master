<?php

namespace app\application\services\dish;

use Yii;
use yii\base\Component;
use app\application\entities\Dish;
use app\application\entities\Ingredient;

class DishAddService extends Component
{
    private const NO_NAME_IN_NEW_DISH = 'Ошибка создания блюда. Не заполнено название';

    private const TOO_FEW_INGREDIENTS_FOR_NEW_DISH = 'Ошибка создания блюда. Слишком мало ингредиентов';

    private const NEW_DISH_SUCCESS_ADD = 'Новое блюдо успешно добавлено';

    private const ERROR_ON_PROCESS_ADD = 'Возникла ошибка при сохранении. Пожалуйста проверьте данные и попробуйте снова';

    public function getCreateWindowData()
    {
        $dish_model = new Dish();

        $manage_dish_ingredients_list = Yii::$app->session->get('manage_dish_ingredients') ?? [];
        $message = Yii::$app->request->get()['message'] ?? null;

        if ($dish_model->load(Yii::$app->request->post())) {

            $manage_dish_ingredients_list = $this->addNewInfoOnCreateProcess($dish_model, $manage_dish_ingredients_list);
            return [
                'available_ingredient_list'=>Ingredient::getAvailableIngredientList($manage_dish_ingredients_list),
                'dish_model'=>$dish_model,
                'manage_dish_ingredients_list'=>Ingredient::mergeIngredientNamesOnIds($manage_dish_ingredients_list),
                'message'=>$message
            ];

        } else {

            $manage_dish_ingredients_list = $this->checkDiscardItemInIngredientList($manage_dish_ingredients_list);
            return [
                'available_ingredient_list'=>Ingredient::getAvailableIngredientList($manage_dish_ingredients_list),
                'dish_model'=>$dish_model,
                'manage_dish_ingredients_list'=>Ingredient::mergeIngredientNamesOnIds($manage_dish_ingredients_list),
                'message'=>$message
            ];
        }
    }

    public function saveNewDish()
    {
        // Получение данных из сессии (проверка все ли норм)
        $dish_name = Yii::$app->session->get('new_dish_name') ?? null;
        $dish_ingredients = Yii::$app->session->get('manage_dish_ingredients') ?? [];

        if (empty($dish_name)) {
            return '/dish/create?message='.static::NO_NAME_IN_NEW_DISH;
        }

        if (count($dish_ingredients)<static::MIN_QUANTITY_OF_INGREDIENTS) {
            return '/dish/create?message='.static::TOO_FEW_INGREDIENTS_FOR_NEW_DISH;
        }

        Dish::transactionForAdd([
            'dish_name'=>$dish_name,
            'dish_ingredients'=>$dish_ingredients
        ]);

        $this->sessionClearAfterAdd();

        return '/admin?message='.static::NEW_DISH_SUCCESS_ADD;
    }

    private function sessionClearAfterAdd()
    {
        Yii::$app->session->remove('manage_dish_ingredients');
        Yii::$app->session->remove('new_dish_name');
    }

    public function addNewInfoOnCreateProcess($dish_model, $manage_dish_ingredients_list)
    {
        $modified_dish_ingredients_list = $manage_dish_ingredients_list;

        if (!empty($dish_model->choice_ingredient)) {
            if (!in_array($dish_model->choice_ingredient, $modified_dish_ingredients_list)) {
                $modified_dish_ingredients_list[] = $dish_model->choice_ingredient;
            }
        }
        Yii::$app->session->set('manage_dish_ingredients', $modified_dish_ingredients_list);
        Yii::$app->session->set('new_dish_name', $dish_model->name ?? "");

        return $modified_dish_ingredients_list;
    }

    public static function checkDiscardItemInIngredientList($manage_dish_ingredients_list)
    {
        $modified_dish_ingredients_list = $manage_dish_ingredients_list;

        if (!empty(Yii::$app->request->get()['discard'])) {

            $key = array_search(Yii::$app->request->get()['discard'], $modified_dish_ingredients_list);

            if ($key !== false) {
                unset($modified_dish_ingredients_list[$key]);
                Yii::$app->session->set('manage_dish_ingredients', $modified_dish_ingredients_list);
            }
        }

        return $modified_dish_ingredients_list;
    }
}
