<?php
use yii\helpers\Html;
?>

<?php
    echo Html::ul($menu_items, ['item' => function($item, $index) {
              return Html::tag('li', Html::a($index, $item, ['class'=>'standart-button']));
                    }, 'class'=>'main-menu']);
?>
