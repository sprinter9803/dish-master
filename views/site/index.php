<?php
$this->title = 'Мастер кухни';

$menu_items = [
    'Книги рецептов'=>'/books',
    'Поиск по ингредиентам'=>'/search'
];
?>

<?= $this->render('_main_menu', [
        'menu_items'=>$menu_items
]); ?>
