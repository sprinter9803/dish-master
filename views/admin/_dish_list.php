<?php
use app\helpers\UrlTransform;
use yii\helpers\Html;
use yii\grid\GridView;
?>

<?php
    echo GridView::widget([
        'dataProvider'=>$dish_index_data_provider,
        'columns'=>[
            [
                'attribute'=>'id',
                'headerOptions'=>['width'=>40],
                'contentOptions'=>[
                    'class'=>'admin-grid-id-param'
                ]
            ],
            [
                'attribute'=>'name',
                'label'=>'Название',
                'contentOptions'=>[
                    'class'=>'admin-grid-name-param'
                ]
            ],
            [
                'class'=>'yii\grid\ActionColumn',
                'headerOptions'=>['width'=>50],
                'template'=>'{update}{delete}',
                'buttons'=>[
                    'update'=>function ($url, $model) {
                        return Html::a('&#9998;', 'dish/update/'.UrlTransform::getIdFromUrl($url), ['class'=>'grid-table-button']);
                    },
                    'delete'=>function ($url, $model) {
                        return Html::a('&#10006;', 'dish/delete/'.UrlTransform::getIdFromUrl($url), ['class'=>'grid-table-button']);
                    }
                ]
            ]
        ],
        'summaryOptions'=>[
            'class'=>'admin-grid-summary'
        ],
        'layout'=>'{summary}{items}<div class="catalog-tile-pager">{pager}</div>',
        'rowOptions'=>[
            'class'=>'admin-grid-row'
        ],
        'tableOptions'=>[
            'class'=>'admin-grid-table'
        ],
        'options'=>[
            'class'=>'admin-grid-container'
        ]
    ]);
?>
