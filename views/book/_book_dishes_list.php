<?php
use app\helpers\HtmlClassTransform;
?>

<div class="single-book-dishes-list">
    <?php foreach ($dishes as $dish): ?>
        <a class="books-item-common-style single-book-dishes-list-item clearfix" href="/dish/<?= $dish->id ?>">
            <div class="book-dishes-list-item-name">
                <?= $dish->name ?>
            </div>
            <div class="book-dishes-list-item-complexity <?= HtmlClassTransform::getComplexityBackgroundClassName($dish->complexity) ?>">

            </div>
            <div class="book-dishes-list-item-description">
                <?= $dish->description ?>
            </div>
        </a>
    <?php endforeach; ?>
</div>
