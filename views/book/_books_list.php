<div class="books-list">
    <?php foreach($books as $book): ?>
        <a class="books-list-item books-item-common-style" href="/book/<?= $book->id ?>">
            <div class="book-list-item-name">
                <?= $book->name ?>
            </div>
            <div>
                Language: <?= $languages[$book->language] ?>
            </div>
            <div>
                Year: <?= date_parse($book->year)['year'] ?>
            </div>
            <div>
                Description: <?= $book->description ?>
            </div>
        </a>
    <?php endforeach; ?>
</div>
