<?php
$this->title = 'Книги рецептов';
?>

<?= $this->render('_books_list', [
        'books'=>$books,
        'languages'=>$languages
]); ?>
