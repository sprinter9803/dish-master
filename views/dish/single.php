<?php
use app\helpers\HtmlClassTransform;
$this->title = '"'.$dish['info']->name.'"';
?>

<div class="single-dish-content-container">
    <div class="single-dish-content-name">
        <h3><?= $dish['info']->name ?></h3>
    </div>
    <div class="single-dish-content-description">
        <?= $dish['info']->description ?>
    </div>
    <div class="single-dish-content-info-label">
        Книга рецептов: <?= $dish['book']->name ?>
    </div>
    <div class="single-dish-content-info-label">
        Сложность: <span class="<?= HtmlClassTransform::getComplexityColorClassName($dish['info']->complexity) ?>">
            <?= HtmlClassTransform::getComplexityReadableName($dish['info']->complexity) ?>
        </span>
    </div>
    <div class="single-dish-content-ingredients-label">
        Ингредиенты:
    </div>
    <div>
        <?php foreach($dish['ingredients'] as $ingredient): ?>
            <div class="single-dish-ingredient-item">
                &#9658; <span class="single-dish-ingredient-name"><?= $ingredient->name ?></span>
            </div>
        <?php endforeach; ?>
    </div>
</div>
