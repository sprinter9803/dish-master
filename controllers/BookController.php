<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\application\entities\Book;
use app\application\entities\Dish;

class BookController extends Controller
{
    private const LANGUAGE_ALIAS_LIST = [
        'ru'=>'Russian',
        'en'=>'English'
    ];

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionList()
    {
        return $this->render('list', [
            'books'=>Book::getFullBooksList(),
            'languages'=>static::LANGUAGE_ALIAS_LIST
        ]);
    }


    public function actionSingle($id)
    {
        return $this->render('single', [
            'dishes'=>Book::getBookDishes($id),
            'book_name'=>Book::getBookName($id)
        ]);
    }
}
