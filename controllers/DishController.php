<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\behaviors\FindModelBehavior;
use app\behaviors\ResponseActionBehavior;
use app\application\services\dish\DishAddService;
use app\application\services\dish\DishUpdateService;
use app\application\entities\Dish;

class DishController extends Controller
{
    private const NEW_DISH_SUCCESS_ADD = 'Новое блюдо успешно добавлено';

    private const NEW_DISH_ERROR_ADD = 'Произошла ошибка при добавлении нового блюда';

    private const DISH_SUCCESS_DELETE = 'Блюдо успешно удалено';

    private const UPDATE_DISH_WITHOUT_NAME = 'Ошибка при обновлении. У блюда должно быть название';

    private const TOO_FEW_INGREDIENTS_FOR_UPDATE_DISH = 'Ошибка при обновлении. У блюда остается слишком мало ингредиентов';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['@'],
                    ]
                ]
            ],
            'FindModelBehavior'=>FindModelBehavior::className(),
            'ResponseActionBehavior'=>ResponseActionBehavior::className(),
        ];
    }

    public function actionCreate()
    {
        $add_data_service = new DishAddService();
        return $this->render('create', $add_data_service->getCreateWindowData());
    }

    public function actionUpdate($id)
    {
        $update_data_service = new DishUpdateService();
        return $this->responseAction($update_data_service->getUpdateWindowData($id));
    }

    public function actionDelete($id)
    {
        $this->findModel($id, new Dish())->delete();
        return $this->redirect(['/admin?message='.static::DISH_SUCCESS_DELETE]);
    }

    public function actionView($id)
    {
        return $this->render('single', [
            'dish'=>Dish::getDishData($id)
        ]);
    }

    public function actionSave()
    {
        return $this->redirect(Dish::saveNewDish());
    }
}
