<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\behaviors\ResponseActionBehavior;
use app\application\services\ingredient\IngredientManageService;

class IngredientController extends Controller
{
    private $ingredient_manage_service;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['@'],
                    ]
                ]
            ],
            'ResponseActionBehavior'=>ResponseActionBehavior::className(),
        ];
    }

    public function beforeAction($action)
    {
        $this->ingredient_manage_service = new IngredientManageService();
        return parent::beforeAction($action);
    }

    public function actionCreate()
    {
        return $this->responseAction($this->ingredient_manage_service->createNewIngredient());
    }

    public function actionUpdate($id)
    {
        return $this->responseAction($this->ingredient_manage_service->updateDataForIngredient($id));
    }

    public function actionDelete($id)
    {
        return $this->redirect($this->ingredient_manage_service->deleteIngredient($id));
    }
}
