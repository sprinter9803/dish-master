<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m200805_175724_create_dish
 */
class m200805_175724_create_dish extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dish}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
            'description'=>Schema::TYPE_TEXT,
            'complexity'=>Schema::TYPE_TINYINT. ' NOT NULL',
            'book_id'=>Schema::TYPE_INTEGER
        ], $tableOptions);

        $this->insert('{{%dish}}', [
            'id'=>1,
            'name'=>'Vegetable salad', // Tomate Cucumber Potato
            'description'=>'Simple and delicious summer cuisine salad. Requires few ingredient and is quick to cook',
            'complexity'=>1,
            'book_id'=>1
        ]);

        $this->insert('{{%dish}}', [
            'id'=>2,
            'name'=>'Quick lunch', // Beef Potato Butter Dill
            'description'=>'Dish two',
            'complexity'=>2,
            'book_id'=>1
        ]);

        $this->insert('{{%dish}}', [
            'id'=>3,
            'name'=>'Fruit dream', // Orange Milk Apple Peach Apricot
            'description'=>'An exquisite and unusual salad. Will give you fruit delight',
            'complexity'=>3,
            'book_id'=>2
        ]);

        $this->insert('{{%dish}}', [
            'id'=>4,
            'name'=>'Simple vegetable salad', // Tomate Cucumber
            'description'=>'Dish four',
            'complexity'=>1,
            'book_id'=>null
        ]);

        $this->insert('{{%dish}}', [
            'id'=>5,
            'name'=>'Cheese set', // Cheese Beef Dill Egg Mayo
            'description'=>'Dish five',
            'complexity'=>2,
            'book_id'=>3
        ]);

        $this->insert('{{%dish}}', [
            'id'=>6,
            'name'=>'Buckwheat with meat', //
            'description'=>'Dish six',
            'complexity'=>3,
            'book_id'=>1
        ]);

        $this->insert('{{%dish}}', [
            'id'=>7,
            'name'=>'Squid with vegatables', // Squid Seaweed Cucumber Cheese Mayo
            'description'=>'Dish seven',
            'complexity'=>3,
            'book_id'=>3
        ]);

        $this->insert('{{%dish}}', [
            'id'=>8,
            'name'=>'Pepper on fish', // Pepper pollock dill oatmeal
            'description'=>'Tasty fish dish, traditional Norwey cook',
            'complexity'=>2,
            'book_id'=>2
        ]);

        $this->insert('{{%dish}}', [
            'id'=>9,
            'name'=>'Beef with buckwheat', // Beef Buckwheat Tomato
            'description'=>'Easy and nourishing dish',
            'complexity'=>1,
            'book_id'=>null
        ]);

        $this->insert('{{%dish}}', [
            'id'=>10,
            'name'=>'Sea king', // Sturgeon Seaweed Cucumber Oatmeel Rice
            'description'=>'Famous and tasty dish of asian cook',
            'complexity'=>3,
            'book_id'=>null
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('dish');
    }
}
