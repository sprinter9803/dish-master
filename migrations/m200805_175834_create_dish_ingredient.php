<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m200805_175834_create_dish_ingredient
 */
class m200805_175834_create_dish_ingredient extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dish_ingredient}}', [
            'id'=>Schema::TYPE_PK,
            'dish_id'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'ingredient_id'=>Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);

        $this->addForeignKey('fk-dish_ingredient_id-ingredient_id', '{{%dish_ingredient}}', 'ingredient_id', '{{%ingredient}}', 'id', 'RESTRICT');
        $this->addForeignKey('fk-dish_ingredient_id-dish_id', '{{%dish_ingredient}}', 'dish_id', '{{%dish}}', 'id', 'CASCADE');

        // Vegetable salad
        $this->insert('{{%dish_ingredient}}', [
           'id'=>1,
           'dish_id'=>1,
           'ingredient_id'=>6
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>2,
           'dish_id'=>1,
           'ingredient_id'=>7
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>3,
           'dish_id'=>1,
           'ingredient_id'=>8
        ]);

        // Quick lunch
        $this->insert('{{%dish_ingredient}}', [
           'id'=>4,
           'dish_id'=>2,
           'ingredient_id'=>2
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>5,
           'dish_id'=>2,
           'ingredient_id'=>8
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>6,
           'dish_id'=>2,
           'ingredient_id'=>9
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>7,
           'dish_id'=>2,
           'ingredient_id'=>13
        ]);

        // Fruit dream
        $this->insert('{{%dish_ingredient}}', [
           'id'=>8,
           'dish_id'=>3,
           'ingredient_id'=>4
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>9,
           'dish_id'=>3,
           'ingredient_id'=>10
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>10,
           'dish_id'=>3,
           'ingredient_id'=>11
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>11,
           'dish_id'=>3,
           'ingredient_id'=>14
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>12,
           'dish_id'=>3,
           'ingredient_id'=>15
        ]);

        // Simple vegetable salad
        $this->insert('{{%dish_ingredient}}', [
           'id'=>13,
           'dish_id'=>4,
           'ingredient_id'=>6
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>14,
           'dish_id'=>4,
           'ingredient_id'=>7
        ]);

        // Cheese set
        $this->insert('{{%dish_ingredient}}', [
           'id'=>15,
           'dish_id'=>5,
           'ingredient_id'=>5
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>16,
           'dish_id'=>5,
           'ingredient_id'=>2
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>17,
           'dish_id'=>5,
           'ingredient_id'=>9
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>18,
           'dish_id'=>5,
           'ingredient_id'=>3
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>19,
           'dish_id'=>5,
           'ingredient_id'=>16
        ]);

        // Buckwheat with meat
        $this->insert('{{%dish_ingredient}}', [
           'id'=>20,
           'dish_id'=>6,
           'ingredient_id'=>24
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>21,
           'dish_id'=>6,
           'ingredient_id'=>20
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>22,
           'dish_id'=>6,
           'ingredient_id'=>17
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>23,
           'dish_id'=>6,
           'ingredient_id'=>18
        ]);

        // Squid with vegetables
        $this->insert('{{%dish_ingredient}}', [
           'id'=>24,
           'dish_id'=>7,
           'ingredient_id'=>19
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>25,
           'dish_id'=>7,
           'ingredient_id'=>23
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>26,
           'dish_id'=>7,
           'ingredient_id'=>7
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>27,
           'dish_id'=>7,
           'ingredient_id'=>5
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>28,
           'dish_id'=>7,
           'ingredient_id'=>16
        ]);

        // Pepper on fish
        $this->insert('{{%dish_ingredient}}', [
           'id'=>29,
           'dish_id'=>8,
           'ingredient_id'=>26
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>30,
           'dish_id'=>8,
           'ingredient_id'=>21
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>31,
           'dish_id'=>8,
           'ingredient_id'=>9
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>32,
           'dish_id'=>8,
           'ingredient_id'=>12
        ]);

        // Beef with buckwheat
        $this->insert('{{%dish_ingredient}}', [
           'id'=>33,
           'dish_id'=>9,
           'ingredient_id'=>2
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>34,
           'dish_id'=>9,
           'ingredient_id'=>25
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>35,
           'dish_id'=>9,
           'ingredient_id'=>6
        ]);

        //
        $this->insert('{{%dish_ingredient}}', [
           'id'=>36,
           'dish_id'=>10,
           'ingredient_id'=>22
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>37,
           'dish_id'=>10,
           'ingredient_id'=>23
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>38,
           'dish_id'=>10,
           'ingredient_id'=>7
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>39,
           'dish_id'=>10,
           'ingredient_id'=>12
        ]);
        $this->insert('{{%dish_ingredient}}', [
           'id'=>40,
           'dish_id'=>10,
           'ingredient_id'=>25
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('dish_ingredient');
    }
}
