<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m200805_175843_create_user
 */
class m200805_175843_create_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'id'=>Schema::TYPE_PK,
            'username'=>Schema::TYPE_STRING . ' NOT NULL',
            'password_hash'=>Schema::TYPE_STRING . ' NOT NULL',
            'email'=>Schema::TYPE_STRING,
        ], $tableOptions);

        $this->insert('{{%users}}', [
           'id'=>1,
           'username'=>'admin',
           'password_hash'=>'21232f297a57a5a743894a0e4a801fc3' // Hash для 'admin'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
