<?php

use yii\db\Schema;
use yii\db\Migration;

class m200823_073106_create_author extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%author}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
            'phone'=>Schema::TYPE_STRING,
            'email'=>Schema::TYPE_STRING,
        ], $tableOptions);

        $this->insert('{{%author}}', [
           'id'=>1,
           'name'=>'Publishing house KingBook',
           'phone'=>'89004005353',
           'email'=>'info@kingbook.com',
        ]);

        $this->insert('{{%author}}', [
           'id'=>2,
           'name'=>'Publishing house Eurogate',
           'phone'=>'89007008010',
           'email'=>'sales@eurogate.com',
        ]);

        $this->insert('{{%author}}', [
           'id'=>3,
           'name'=>'Viktor Orlov',
           'phone'=>'89208005012',
           'email'=>'vik.5080@yandex.ru',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('author');
    }
}
