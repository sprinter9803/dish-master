<?php

use yii\db\Schema;
use yii\db\Migration;

class m200817_202032_create_book extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%book}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
            'language'=>Schema::TYPE_STRING . ' NOT NULL',
            'year'=>Schema::TYPE_DATE. ' NOT NULL',
            'author_id'=>Schema::TYPE_INTEGER. ' NOT NULL',
            'quantity_pages'=>Schema::TYPE_INTEGER. ' NOT NULL',
            'link_picture'=>Schema::TYPE_STRING. ' NOT NULL',
            'description'=>Schema::TYPE_TEXT,
        ], $tableOptions);

        $this->insert('{{%book}}', [
           'id'=>1,
           'name'=>'100 tasty dishes of Russian cook',
           'language'=>'ru',
           'year'=>'2020-04-05',
           'author_id'=>1,
           'quantity_pages'=>368,
           'link_picture'=>'/uploads/books/book_one.jpg',
           'description'=>'Many many many various dishes from rich russian cook. Meat, vegatables and other food'
        ]);

        $this->insert('{{%book}}', [
           'id'=>2,
           'name'=>'European traditional cooks',
           'language'=>'en',
           'year'=>'2015-08-09',
           'author_id'=>2,
           'quantity_pages'=>215,
           'link_picture'=>'/uploads/books/book_two.jpg',
           'description'=>'Book of traditional european cook from different nations. Salads, soups, lunches and other dishes/'
        ]);

        $this->insert('{{%book}}', [
           'id'=>3,
           'name'=>'250 Asian dishes',
           'language'=>'en',
           'year'=>'2012-12-01',
           'author_id'=>3,
           'quantity_pages'=>500,
           'link_picture'=>'/uploads/books/book_three.jpg',
           'description'=>'Asian dishes for you enjoy/ Sea cook, and various exotic food.'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('book');
    }
}
