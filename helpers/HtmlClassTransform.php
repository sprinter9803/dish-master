<?php

namespace app\helpers;

class HtmlClassTransform
{
    private const SIMPLE_DISH_COMPLEXITY_NAME = 'Просто';

    private const MEDIUM_DISH_COMPLEXITY_NAME = 'Средне';

    private const HARD_DISH_COMPLEXITY_NAME = 'Сложно';

    public static function getComplexityBackgroundClassName($complexity_quant)
    {
        switch ($complexity_quant) {
            case SIMPLE_DISH_COMPLEXITY:
                return 'simple-dish-marker-complexity';
                break;
            case MEDIUM_DISH_COMPLEXITY:
                return 'medium-dish-marker-complexity';
                break;
            case HARD_DISH_COMPLEXITY:
                return 'hard-dish-marker-complexity';
                break;
            default:
                return '';
                break;
        }
    }

    public static function getComplexityColorClassName($complexity_quant)
    {
        switch ($complexity_quant) {
            case SIMPLE_DISH_COMPLEXITY:
                return 'simple-dish-complexity-color';
                break;
            case MEDIUM_DISH_COMPLEXITY:
                return 'medium-dish-complexity-color';
                break;
            case HARD_DISH_COMPLEXITY:
                return 'hard-dish-complexity-color';
                break;
            default:
                return '';
                break;
        }
    }

    public static function getComplexityReadableName($complexity_quant)
    {
        switch ($complexity_quant) {
            case SIMPLE_DISH_COMPLEXITY:
                return static::SIMPLE_DISH_COMPLEXITY_NAME;
                break;
            case MEDIUM_DISH_COMPLEXITY:
                return static::MEDIUM_DISH_COMPLEXITY_NAME;
                break;
            case HARD_DISH_COMPLEXITY:
                return static::HARD_DISH_COMPLEXITY_NAME;
                break;
            default:
                return '';
                break;
        }
    }
}
