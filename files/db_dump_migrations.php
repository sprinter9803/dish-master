// dish
$this->createTable('{{%dish}}', [
    'id' => $this->primaryKey(),
    'name' => $this->string(255)->notNull(),
], $this->tableOptions);

// dish_ingredient
$this->createTable('{{%dish_ingredient}}', [
    'id' => $this->primaryKey(),
    'dish_id' => $this->integer(11)->notNull()->unique(),
    'ingredient_id' => $this->integer(11)->notNull(),
], $this->tableOptions);

// ingredient
$this->createTable('{{%ingredient}}', [
    'id' => $this->primaryKey(),
    'name' => $this->string(255)->notNull(),
    'hidden' => $this->boolean()->notNull(),
], $this->tableOptions);

// users
$this->createTable('{{%users}}', [
    'id' => $this->primaryKey(),
    'username' => $this->string(255)->notNull(),
    'password_hash' => $this->string(255)->notNull(),
    'email' => $this->string(255)->null(),
], $this->tableOptions);

// fk: dish_ingredient
$this->addForeignKey('fk_dish_ingredient_dish_id', '{{%dish_ingredient}}', 'dish_id', '{{%dish}}', 'id');
$this->addForeignKey('fk_dish_ingredient_ingredient_id', '{{%dish_ingredient}}', 'ingredient_id', '{{%ingredient}}', 'id');

